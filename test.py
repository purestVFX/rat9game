import asyncio

from aiohttp import web

import socketio
import random
import time

sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)


things = []
duration = 0.0
world_size = ws = 2000
world_time = wt = 1

def add_random():
    random_pos = [random.randint(-ws,ws), random.randint(-ws,ws)]
    random_vel = [random.randint(-10,10), random.randint(-10,10)]

    things.append([random_pos,random_vel])

def update_world():
    wt += 1
    for thing in things:
        
        # first check edge collisions, reverse velocity on edge.
        if thing[0][0] > ws or thing[0][0] < -ws:
            thing[1][0] = -thing[1][0]
        if thing[0][1] > ws or thing[0][1] < -ws:
            thing[1][1] = -thing[1][1]

        # now update possitions.
        thing[0][0]+=thing[1][0]
        thing[0][1]+=thing[1][1]

async def run_world_updates():
    global duration
    while True:
        start_time = time.time()
        update_world()
        duration = time.time() - start_time
        await sio.sleep(0.01-duration)


async def background_task():
    """Example of how to send server generated events to clients."""
    global things
    count = 0
    while True:
        #await sio.sleep(0.04)  #approx 25 fps
        await sio.sleep(1)

        count += 1
        # await sio.emit('my response', {'data': 'Server generated event'},
        #                namespace='/test')
        await sio.emit('world update', {'data': things},
                        namespace='/test')


async def index(request):
    with open('test.html') as f:
        return web.Response(text=f.read(), content_type='text/html')


@sio.on('my event', namespace='/test')
async def test_message(sid, message):
    await sio.emit('my response', {'data': message['data']}, room=sid,
                   namespace='/test')


@sio.on('my broadcast event', namespace='/test')
async def test_broadcast_message(sid, message):
    await sio.emit('my response', {'data': message['data']}, namespace='/test')


@sio.on('join', namespace='/test')
async def join(sid, message):
    sio.enter_room(sid, message['room'], namespace='/test')
    await sio.emit('my response', {'data': 'Entered room: ' + message['room']},
                   room=sid, namespace='/test')


@sio.on('leave', namespace='/test')
async def leave(sid, message):
    sio.leave_room(sid, message['room'], namespace='/test')
    await sio.emit('my response', {'data': 'Left room: ' + message['room']},
                   room=sid, namespace='/test')


@sio.on('close room', namespace='/test')
async def close(sid, message):
    await sio.emit('my response',
                   {'data': 'Room ' + message['room'] + ' is closing.'},
                   room=message['room'], namespace='/test')
    await sio.close_room(message['room'], namespace='/test')


@sio.on('my room event', namespace='/test')
async def send_room_message(sid, message):
    await sio.emit('my response', {'data': message['data']},
                   room=message['room'], namespace='/test')


@sio.on('disconnect request', namespace='/test')
async def disconnect_request(sid):
    await sio.disconnect(sid, namespace='/test')


@sio.on('connect', namespace='/test')
async def test_connect(sid, environ):
    add_random()
    await sio.emit('my response', {'data': 'Connected', 'count': 0}, room=sid,
                   namespace='/test')


@sio.on('disconnect', namespace='/test')
def test_disconnect(sid):
    print('Client disconnected')


app.router.add_static('/static', 'static')
app.router.add_get('/', index)


if __name__ == '__main__':
    sio.start_background_task(background_task)
    sio.start_background_task(run_world_updates)
    web.run_app(app)
