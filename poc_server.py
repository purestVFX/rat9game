# gameserver test version
import asyncio

from aiohttp import web

import socketio
import random
import time



players = []
bullets = []
worldtime = 1

class game_object:
    "represent a game object"
    def __init__(self):
        self.type = "bullet"
        self.px = 0
        self.py = 0
        self.vx = 0
        self.vy = 0

    def increment(self):
        self.px += self.vx
        self.py += self.vy

class ship(game_object):
    def __init__(self, id):
        super(ship, self).__init__()
        self.type = "player"
        self.playerID = id
        self.r = 0

def package_world():
    global worldtime
    global players
    world = {"time": worldtime, "ships":[], "bullets": []}
    for aship in players:
        shipDict = {"px":aship.px, "py":aship.py,"vx":aship.vx,"vy":aship.vy,"r":aship.r}
        world["ships"].append(shipDict)
    return world

###### world update
def update_world():
    global worldtime
    global players
    worldtime += 1
    for aship in players:
        aship.increment()
    
async def run_world_updates():
    while True:
        start_time = time.time()
        update_world()
        duration = time.time() - start_time
        await sio.sleep(0.01-duration)

################ coms:

sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)

#provide client code html:
async def index(request):
    with open('poc_client.html') as f:
        return web.Response(text=f.read(), content_type='text/html')

app.router.add_get('/', index)


async def send_world():
    """send the clients the location of everything once a second"""
    count = 0
    while True:
        await sio.sleep(1)

        await sio.emit('full world', {'data': package_world()},
                        namespace='/poc')


@sio.on('connect', namespace='/poc')
async def poc_connect(sid, environ):
    print('Client connected')

    newPlayer = ship(sid)
    players.append(newPlayer)
    
    await sio.emit('my response', {'data': 'Connected', 'count': 0}, room=sid,
                   namespace='/poc')

@sio.on('disconnect', namespace='/poc')
def poc_disconnect(sid):
    print('Client disconnected')

#####################################

if __name__ == '__main__':
    
    sio.start_background_task(run_world_updates)
    sio.start_background_task(send_world)
    web.run_app(app)
